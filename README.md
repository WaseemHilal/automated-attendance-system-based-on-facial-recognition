# README #

* Thank you for being here first. 
* This was my MCA final semester project.
* The project is a solution for manual and time consuming attendance tasks.

### What is this repository for? ###

* In this project we have implemented the automated attendance system using MATLAB. We have projected our ideas to implement “automated attendance system based on facial recognition”, in which it imbibes large applications. The application includes face identification, which saves time and eliminates chances of proxy attendance because of the face authorization. Hence, this system can be implemented in a field where attendance plays an important role.

### How do I get set up? ###

* As we know the project is built on MATLAB platform so we need MATLAB installed in a system having atleat RAM that can run software.
* The software is built in Windows 10 environment, it uses MySQL for database purposes.
* To run this application we need to run the program file(.m) "dashboard.fig" that that will guide you afterwards.


### Who do I talk to? ###

Profile |
------------ | -------------
Waseem Hilal | ![GitHub Logo](https://scontent.fdel11-1.fna.fbcdn.net/v/t1.0-1/p160x160/16114406_1422144824503022_3787834606372713146_n.jpg?oh=8caf3789bb221aad39d55c916ed2db4d&oe=5A6B670D)
MCA(2017) Islamic University of Science and Technology | Link to my [LinkedIn profile](https://linkedin.com/in/waseemhilal)
waseemhilal10@gmail.com |
+91-7006077173 |

  

	